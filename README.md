### Race 3D display ###

Proyecto final de la asignatura "Realidad Virtual" de 3º del grado en Ingeniería Informática en la Universidad de Huelva.

Entorno visual en 3d utilizando canvas y webGL, para representar un circuito durante una carrera, a partir de un fichero en formato json, obtenido de la ejecución de una carrera en el torneo hwo20014.
* v0.0.1

### How do I get set up? ###

Clona el repositorio en una carpeta local y abre con el navegador (Mozilla) el fichero index.html.

### Documentation ###

* http://3d-race-display.readthedocs.org/es/latest/index.html

### Who do I talk to? ###

* César Antonio Enrique Ramírez - caenrique93@gmail.com
