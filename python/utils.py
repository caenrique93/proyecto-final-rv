# -*- coding: utf-8 -*-
import math
import json
import fileinput
""" 
    Funciones auxiliares utilizadas para el calculo de ciertos valores (como traduccion de coordenadas)
    y para leer/escribir informacion de ficheros.
"""


def polares(punto, origen):
    """ 
        Transforma de coordenadas cartesianas a coordenadas polares.

        :param punto punto: El punto del cual queremos las coordenadas polares.
        :param punto origen: El punto con respecto al cual queremos expresadas las coodenadas de *punto*\ .
        :return: (angulo, radio) El ángulo que forma el vector *origen-punto* con respecto
            al eje horizontal positivo, y la distancia entre *origen* y *punto*\ .
        :rtype: (float, float)
    """

    dx = punto[0] - origen[0]
    dy = punto[1] - origen[1]
    radio = math.sqrt(dx**2 + dy**2)
    angulo = math.degrees(math.atan2(dy, dx))
    return (angulo, radio)


def cartesianas(angulo, radio, origen):
    """
        Transforma de coordenadas polares a coordenadas cartesianas.

        :param float angulo: El angulo que forma el vector *origen-punto* con respecto 
            al eje horizontal positivo.
        :param float radio: La distancia entre *origen* y el punto.
        :param punto origen: Origen con respecto al cual queremos las coordenadas del punto.
        :return: El punto en coordenadas cartesianas.
        :rtype: punto
    """
    punto = []
    punto.append((radio * math.cos(math.radians(angulo))) + origen[0])  # coordenada X
    punto.append((radio * math.sin(math.radians(angulo))) + origen[1])  # coordenada Y
    punto.append(0)  # coordenada z
    return punto


def ancho(lineas):
    """
        Calula el ancho de las piezas del circuito.

        :param lineas: Información sobre cuántos carriles tiene la pista en formato json.
        :return: El ancho de la pista.
        :rtype: int
    """
    mayor = menor = 0
    for linea in lineas:
        distancia = float(linea['distanceFromCenter'])
        if distancia > mayor:
            mayor = distancia
        if distancia < menor:
            menor = distancia
    return mayor - menor


def generarJSON(name, data):
    """
        Genera un fichero JSON con la información de *data*\ .

        :param str name: Nombre del fichero.
        :param data: Contiene la información que se quiere añadir al fichero JSON.
    """
    f = open("../models/"+name+".json", "w")
    resultado = json.dumps(data)
    f.write(resultado)
    f.close()


def data_from_JSON(filename):
    """
        Carga información de un fichero en formato JSON.

        :param str filename: Nombre del fichero.
        :return: La información cargada desde el fichero.
    """
    return [line.replace('\n', '') for line in fileinput.input("../tracks/" + filename + ".json")]


def piezas_from_data(data):
    """
        Descompone *data* y extrae la información que es necesaria para el modelado de la pista.

        :param data: Información de la pista en formato JSON.
        :return: (piezas, anchoPiezas) Las piezas de la pista y el ancho.
        :rtype: (array, int)
    """
    json_decoded = json.loads(data[4])
    json_piezas = json_decoded['data']['race']['track']['pieces']
    json_carriles = json_decoded['data']['race']['track']['lanes']
    anchoPiezas = ancho(json_carriles)
    return (json_piezas, anchoPiezas)
