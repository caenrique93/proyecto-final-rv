#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import utils
import modelador


def main():
    """
        Punto de entrada. Se encarga de leer el fichero cuyo nombre recibe como parametro, y de
        pasarselo a la clase Modelador.
    """
    if len(sys.argv) >= 2:
        track_name = sys.argv[1]
    else:
        print("introduce el nombre de la pista")
        return

    json_data = utils.data_from_JSON(track_name)

    data = utils.piezas_from_data(json_data)

    piezas = data[0]
    ancho = data[1]

    mod = modelador.Modelador()
    mod.modelar_3d(piezas, ancho, track_name, 4)


if __name__ == '__main__':
    main()
