# -*- coding: utf-8 -*-
import utils


class Modelador:
    """
        Clase encargada de modelar el circuito a partir de una
        especificacion en formato JSON.
    """

    def __init__(self, color=[0.0, 0.0, 0.0], resolucion=10):
        """
            Constructor de la clase Modelador.

            :param color: el color del modelo.
            :param int resolucion: la resolucion con la que se dibujaran las
                curvas.
        """
        self.json_resultado = {
            'vertices': [],
            'indices': [],
            'color': color
        }
        self.resolucion = resolucion

    def ultimosPuntos(self):
        """
            Devuelve los dos últimos puntos que se encuntran en *json_resultado['vertices']*
            en el instante de la llamada.

            :return: dos puntos
            :rtype: (punto, punto)
        """
        a = self.json_resultado['vertices']
        tam = len(a)
        punto1 = a[tam-6:tam-3]
        punto2 = a[tam-3:]
        return (punto1, punto2)

    def submit(self, vertices):
        """
            Agrega los puntos dados en *vertices* a *json_resultado['vertices']*

            :param vertices: lista de vértices que componen la figura
            :type vertices: array de puntos
        """
        lPuntos = []
        for i in vertices:
            lPuntos.append(i[0])
            lPuntos.append(i[1])
            lPuntos.append(i[2])

        self.json_resultado['vertices'].extend(lPuntos)

    def unirFinal(self):
        """
            Elimina los últimos puntos de la figura que coinciden con los primeros
            y añade las aristas necesarias.
        """
        tam = len(self.json_resultado['vertices'])
        self.json_resultado['vertices'] = self.json_resultado['vertices'][:tam - 6]

        tam = (tam - 6) / 3
        self.json_resultado['indices'].extend([tam-2, tam-1, 1, 0, tam-2, 1])

    def unirFinal3d(self):
        """
            Elimina los últimos puntos del a figura que coinciden con los primeros
            y añade las aristas necesarias para un modelo en 3 dimensiones.
        """
        tam = len(self.json_resultado['vertices'])
        self.json_resultado['vertices'] = self.json_resultado['vertices'][:tam - 12]

        tam = ((tam - 12) / 3) - 1
        self.json_resultado['indices'].extend([tam-3, tam-2, 0, 1, 0, tam-2, tam-1, 2, tam, 3, tam, 2,
                                               tam-1, tam-3, 2, 0, 2, tam-3, tam, 3, tam-2, 1, tam-2, 3])

    def dibujaRecta(self, punto1, punto2, largo):
        """
            Dibuja una pieza recta del circuito.

            :param punto punto1: primer punto de referencia
            :param punto punto2: segundo punto de referencia
            :param int largo: longitud de la pieza
        """
        vertices = []

        pol = utils.polares(punto2, punto1)
        angulo = pol[0] - 90

        vertices.append(utils.cartesianas(angulo, largo, punto1))
        vertices.append(utils.cartesianas(angulo, largo, punto2))

        self.submit(vertices)

    def dibujaCurva(self, punto1, punto2, dangulo, radio):
        """
            Dibuja una pieza curva del circuito

            :param punto punto1: primer punto de referencia
            :param punto punto2: segundo punto de referencia
            :param float dangulo: ángulo que define el arco a dibujar
            :param float radio: radio de la circunferencia a partir de la cual se dibuja el arco
        """
        vertices = []
        puntoPolar = utils.polares(punto2, punto1)
        pangulo = puntoPolar[0]
        ancho_pista = puntoPolar[1]

        if(dangulo > 0):
            radio1 = radio - (ancho_pista / 2)
            radio2 = radio + (ancho_pista / 2)
            alpha = pangulo
            pangulo += 180
        else:
            radio2 = radio - (ancho_pista / 2)
            radio1 = radio + (ancho_pista / 2)
            alpha = pangulo - 180

        centro = utils.cartesianas(pangulo, radio1, punto1)

        dalpha = dangulo / self.resolucion
        for i in range(self.resolucion):
            alpha -= dalpha
            vertices.append(utils.cartesianas(alpha, radio1, centro))
            vertices.append(utils.cartesianas(alpha, radio2, centro))

        self.submit(vertices)

    def generar_indices_2d(self):
        """
            Genera los *indices* a partir de los *vertices* siguiendo un patrón
            para un modelo en 2 dimensiones.
        """
        iteraciones = (len(self.json_resultado['vertices']) / 6) - 2
        indices = []
        for i in range(iteraciones):
            i = i * 2
            indices.extend([i, i+1, i+3, i+2, i, i+3])
        self.json_resultado['indices'].extend(indices)
        self.unirFinal()

    def generar_indices_3d(self):
        """
            genera los *indices* a partir de los *vertices* siguiendo
            un patrón para un modelo en 3 diensiones.
        """
        iteracciones = (len(self.json_resultado['vertices']) / 12) - 2
        indices = []
        for i in range(iteracciones):
            i = i * 4
            indices.extend([i, i+1, i+4, i+5, i+4, i+1, i+2, i+6, i+3, i+7, i+3, i+6,
                            i+2, i, i+6, i+4, i+6, i, i+3, i+7, i+1, i+5, i+1, i+7])
        self.json_resultado['indices'] = indices
        self.unirFinal3d()

    def dar_volumen(self, grosor):
        """
            Añade los vértices necesarios para convertir un modelo 2d de la pista
            en uno 3d.

            :param int grosor: grosor del modelo en el eje Z.
        """
        iteracciones = len(self.json_resultado['vertices']) / 6
        vertices3d = []
        for i in range(iteracciones):
            i = i * 6
            vertices3d.extend(self.json_resultado['vertices'][i:i+6])
            vertices3d.extend(self.json_resultado['vertices'][i:i+2])
            vertices3d.append(-grosor)
            vertices3d.extend(self.json_resultado['vertices'][i+3:i+5])
            vertices3d.append(-grosor)
        self.json_resultado['vertices'] = vertices3d

    def modelado(self, piezas, ancho_pista):
        """
            Modelado básico del circuito. Es llamada por modelar_2d y modelar_3d.

            :param piezas: las piezas del circuito en formato JSON.
            :param int ancho_pista: ancho de la pista.
        """
        puntos = [0, 0, 0, 0, ancho_pista, 0]
        self.json_resultado['vertices'].extend(puntos)

        largo = float(piezas[0]['length'])
        del piezas[0]

        p = self.ultimosPuntos()
        self.dibujaRecta(p[0], p[1], largo)

        for pieza in piezas:
            p = self.ultimosPuntos()
            if "length" in pieza:
                self.dibujaRecta(p[0], p[1], float(pieza['length']))
            else:
                self.dibujaCurva(p[0], p[1], float(pieza['angle']), float(pieza['radius']))

    def modelar_2d(self, piezas, ancho_pista, filename):
        """
            Modelado en 2d del circuito.

            :param piezas: las piezas del circuito en formato JSON.
            :param int ancho_pista: ancho de la pista.
            :param str filename: nombre del archivo resultante.
        """
        self.modelado(piezas, ancho_pista)

        self.generar_indices_2d()
        utils.generarJSON(filename+"2d", self.json_resultado)

    def modelar_3d(self, piezas, ancho_pista, filename, grosor):
        """
            Modelado en 3d del circuito.

            :param piezas: las piezas del circuito en formato JSON.
            :param int ancho_pista: ancho de la pista.
            :param str filename: nombre del archivo resultante.
            :param int grosor: grosor del modelo en el eje Z.
        """
        self.modelado(piezas, ancho_pista)
        self.dar_volumen(grosor)
        self.generar_indices_3d()
        utils.generarJSON(filename+"3d", self.json_resultado)
