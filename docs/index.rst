.. 3D Race Display documentation master file, created by
   sphinx-quickstart on Sat Jun  7 13:42:31 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to 3D Race Display's documentation!
===========================================

Proyecto final de la asignatura "Realidad Virtual" de 3º del grado en Ingeniería Informática en la Universidad de Huelva.

Entorno visual en 3d utilizando canvas y webGL, para representar un circuito durante una carrera, 
a partir de un fichero en formato json, obtenido de la ejecución de una carrera en el torneo hwo20014.

Contents:

.. toctree::
   :maxdepth: 2

   code



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

