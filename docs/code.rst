Auto generated documentation
============================

.. automodule:: modelador

.. autoclass:: Modelador
   :members:

.. automodule:: genPuntos 
   :members:

.. automodule:: utils 
   :members:
